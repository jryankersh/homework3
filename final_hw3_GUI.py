
# ---------------------------------------------------- #
# File: v4home2.py
# ---------------------------------------------------- #
# Author(s): J. Ryan Kersh, Linley Duke, 
#            with BitBucket handle (jryankersh)
# [you may add confluence or github link]
# ---------------------------------------------------- #
# Plaftorm:    Unix
# Environment: Python          2.7.8
# Libaries:    numpy           1.9.0
#              matplotlib     u1.4.0
#       	   scipy          0.14.0
#              sympy            N/A
#              OpenCV          2.4.9
#       	   SciKits Image  0.10.1
# ---------------------------------------------------- #
# Description:
'''<docstring: put here the description of your code>'''
# <include name of other modules used/imported>
# <include name of other modules used/imported>
# ---------------------------------------------------- #


#Creating GUI

from Tkinter import *
import Tkinter as tk
import numpy as np
from numpy import sin, cos, pi, array

import matplotlib.pyplot as plt
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
from matplotlib.figure import Figure
import matplotlib.animation as animation
from final_hw3_simulation import *  # _linley is to note changes in case some changes need to be reverted


# pend = Pendulum( state, pend_args, simulation_time, dt, number_of_links )

# animate_dbl_pend( self )
# plot_dbl_pend_all_var(self)
# pend.plot_dbl_pend_chosen_var( self, chosen_var )

# SIMPLE PENDULUM TESTING

# pend = Pendulum( state, pend_args, simulation_time, dt, number_of_links )
# animate_simp_pend( self )
# plot_simp_pend_all_var( self )
# plot_simp_pend_chosen_var( self, chosen_var )

# get_derivs_simp_pend( self, state, *pend_args)


class Controls(Frame):

    def __init__(self, master=None):
        Frame.__init__(self, master)    
        self.master.title("Parameter Controls")

        self.master = master
        #self.grid()
        #self.pack(side="top", fill="both", expand=True)
#         self.grid_rowconfigure(0, weight=1)
#         self.grid_columnconfigure(0, weight=1)

        self.initUI()
        #self.pack()
        #self.fig = plt.figure()
        self.fig = Figure(figsize=(5,4))
        self.canvas = FigureCanvasTkAgg(self.fig, master=self.master)
        self.canvas.get_tk_widget().grid(column=30, row=0, rowspan=15)
        self.canvas.show()

        self.fig2 = Figure(figsize=(5,4))
        self.canvas2 = FigureCanvasTkAgg(self.fig2, master=self.master)
        self.canvas2.get_tk_widget().grid(column=18, row=0, rowspan=15)

        self.canvas2.show()

    def initUI(self):
        
        # Set Checkbutton for Double Pendulum
        self.CheckVar1 = IntVar()
        self.CheckVar1.set = 0
        C1 = Checkbutton(self.master, text = "  Double Pendulum?", 
                           variable = self.CheckVar1)
        C1.grid( row = 1, column = 0, columnspan = 2, pady = 20)
        labelC1 = Label(self, text = "Check for Double. Uncheck for Single")
        labelC1.grid(row = 1, column = 2, columnspan=2)

        #Variables for entry boxes are the ICs
        self.theta0_1 = IntVar()
        self.theta0_2 = IntVar()
        self.v0_1 = IntVar()
        self.v0_2 = IntVar()
        self.simulation_time = IntVar()

        #Function to add generic entry box
        def Add_entry_box(self, val, label, row):
            val.set = 0
            entry = Entry(self.master, textvariable = val)
            entry.grid( row = row, column = 0, columnspan=2, pady = 5)
            entry.bind("<Return>", self.runHandler)
            label = Label(self.master, text = label)
            label.grid(row = row, column = 2, columnspan=2)

        #Use function for each entry box we are creating
        Add_entry_box(self, self.simulation_time, "Simulation Time", 2)
        Add_entry_box(self, self.theta0_1, "Initial Angle - Mass 1 (deg)", 3)
        Add_entry_box(self, self.v0_1, "Initial Velocity - Mass 1 (deg)", 4)
        Add_entry_box(self, self.theta0_2, "Initial Angle - Mass 2 (deg)", 5)
        Add_entry_box(self, self.v0_2, "Initial Velocity - Mass 2 (deg)", 6)
        

        #Variables for sliders are masses and arm lengths
        self.mass1 = IntVar()
        self.mass2 = IntVar()
        self.arm1 = IntVar()
        self.arm2 = IntVar()

        #Function to add generic slider 
        def Add_Slider(self, val, label, column):
            val.set = 1
            scale = Scale(self.master, variable = val, from_=1, to=10)
            scale.grid(row=7, column=column, pady=10)
            label = Label(self.master, text = label).grid(row=8, column=column)

        Add_Slider(self, self.mass1, "Mass1(kg)", 0)
        Add_Slider(self, self.arm1, "Length1(m)", 1)
        Add_Slider(self, self.mass2, "Mass2(kg)", 2)
        Add_Slider(self, self.arm2, "Length2(m)", 3)


        # End program with stop button
        stop = Button(self.master, text = "Stop", command=self.end_prog)
        stop.grid(row=25, column=0)

        # run button tells runHandler to call the main code in hw3
        run = Button(self.master, text="Run", command=self.runHandler)
        run.grid(row=25, column=1)    

        # plot button tells plotHandler to plot from main code
        plot = Button(self.master, text="Plot", command=self.plotHandler)
        plot.grid(row=25, column=2) 


        # to store the selection
        #self.root = Tk()
        self.drop_var = tk.StringVar()
        self.theta1 = StringVar()
        self.theta1 = "Theta 1"
        self.theta2 = StringVar()
        self.theta2 = 'Theta 2'
        self.thetadot1 = StringVar()
        self.thetadot1 = 'Theta_Dot 1'
        self.thetadot2 = StringVar()
        self.thetadot2 = 'Theta_Dot 2'

        self.xx1 = StringVar()
        self.xx1 = 'X Position - Mass 1'
        self.yy1 = StringVar()
        self.yy1 = 'Y Position - Mass 1'
        self.xx2 = StringVar()
        self.xx2 = 'X Position - Mass 2'
        self.yy2 = StringVar()
        self.yy2 = 'Y Position - Mass 2'
        
        self.clear = StringVar()
        self.clear = 'Clear Plot'

        # drop down menu
        self.options = tk.OptionMenu( self.master, self.drop_var, self.theta1, 
                                  self.theta2, self.thetadot1, self.thetadot2, 
                                  self.xx1, self.yy1, self.xx2, self.yy2, self.clear)
        self.options.grid(row = 0, column = 0, columnspan = 2)
        # select the first option
        self.drop_var.set( self.theta1)
        
        # button to get selection
        self.b = tk.Button(self.master, text="Select", command=self.handle_selection)
        self.b.grid(row=0, column =2, columnspan = 2)
        
        
    def end_prog(self):
       #self.canvas.delete('all')
       self.canvas.get_tk_widget().delete("all")
       self.master.quit() 
    
    def init_state_and_pend_args(self):
        
        self.sim_time = self.simulation_time.get()
        print( "CHECK VAR", self.CheckVar1.get())
        if self.CheckVar1.get() == 1:
            self.links = 2
        elif self.CheckVar1.get() == 0:
            self.links = 1
        else:
            print('Something is VERY wrong here. Please see code.')
        
        self.pend_args = ( self.mass1.get(), self.arm1.get(), self.mass2.get(), 
                         self.arm2.get() )
        self.state = np.array( [self.theta0_1.get(), self.v0_1.get(), self.theta0_2.get(),
                              self.v0_2.get()], dtype='f' )
        self.state *= np.pi/180
        
        self.dt = 0.05


        print('here is THEEEEEEE STTAATE:' , self.state)
        print(' pend args', pend_args) 
        print(' pend args[0]', pend_args[0])
        print(' pend args[1]', pend_args[1])  
        
    
    def handle_selection(self):
        print "You've selected: " + self.drop_var.get()
        
        self.init_state_and_pend_args()
        self.pend = Pendulum( self.state, self.pend_args, 
            self.sim_time, self.dt, self.links)
        print( "state", self.state)
        print( "pend args", self.pend_args)
        print( 'sim time', self.simulation_time)
        print( 'links: ', self.links)

        self.fig2 = plt.figure(figsize = (5,4))
        self.canvas2 = FigureCanvasTkAgg(self.fig2, master=self.master)
        self.canvas2.get_tk_widget().grid(column=18, row=0, rowspan=15)

        if self.links == 2:
            self.pend.plot_dbl_pend_chosen_var( self.drop_var.get())
            
        elif self.links == 1: 
            self.pend.plot_simp_pend_chosen_var( self.drop_var.get())
        else:
            print('Something is VERY wrong here. Please see code.')

        self.canvas2.show()

        
    def runHandler(self, val = 0):

        self.fig.clear()
        self.init_state_and_pend_args()
        self.pend = Pendulum( self.state, self.pend_args, self.sim_time,
         self.dt, self.links)
        print self.state

        if self.links == 2:
            self.ani = self.pend.animate_dbl_pend( self.fig)
#            plot_dbl_pend_all_var( )
        elif self.links == 1: 
            self.ani = self.pend.animate_simp_pend(  self.fig)
#            plot_simp_pend_all_var( self )
        else:
            print('Something is VERY wrong here. Please see code.')

        self.canvas.show()



    def plotHandler(self):
        
        self.init_state_and_pend_args()
        self.pend = Pendulum( self.state, self.pend_args, self.sim_time, 
            self.dt, self.links)

        self.fig2 = plt.figure(figsize = (5,4))
        self.canvas2 = FigureCanvasTkAgg(self.fig2, master=self.master)
        self.canvas2.get_tk_widget().grid(column=18, row=0, rowspan=15)
        
        if self.links == 2:
            self.curve = self.pend.plot_dbl_pend_all_var( )
        elif self.links == 1: 
            self.curve = self.pend.plot_simp_pend_all_var(  )
        else:
            print('Something is VERY wrong here. Please see code.')

        self.canvas2.show()


















        #Setting state and pend_args 
        #Single pendulum has theta2, v2, m2 and armlength2 of zero
#         if self.CheckVar1.get() == 1:
#             self.state = np.array([self.theta0_1.get(), self.v0_1.get(),
#                 self.theta0_2.get(), self.v0_2.get()])*np.pi/180
# 
#             self.pend_args = (self.mass1.get(), self.arm1.get(), 
#                 self.mass2.get(), self.arm2.get())
#         else:
#             self.state = np.array([self.theta0_1.get(), self.v0_1.get(), 0, 0])
# 
#             self.pend_args = (self.mass1.get(), self.arm1.get(), 0, 0)
# 
#         self.fig = plt.figure()
#         canvas = FigureCanvasTkAgg(self.fig, master=self.master)
#         canvas.get_tk_widget().grid(column=6, row=0, rowspan=40)
#         self.ani = v3home3.animate_dbl_pend(self.state, self.pend_args, 
#             self.t, self.dt, self.fig)
# 
#         canvas.show()



def main():
    root = Tk()
    app = Controls(master = root)
    app.mainloop()

    root.destroy()
    

if __name__ == '__main__':
    main()

