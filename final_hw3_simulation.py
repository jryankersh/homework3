
# ---------------------------------------------------- #
# File: v4home2.py
# ---------------------------------------------------- #
# Author(s): J. Ryan Kersh, Linley Duke, 
#            with BitBucket handle (jryankersh)
# [you may add confluence or github link]
# ---------------------------------------------------- #
# Plaftorm:    Unix
# Environment: Python          2.7.8
# Libaries:    numpy           1.9.0
#              matplotlib     u1.4.0
#       	   scipy          0.14.0
#              sympy            N/A
#              OpenCV          2.4.9
#       	   SciKits Image  0.10.1
# ---------------------------------------------------- #
# Description:
'''<docstring: put here the description of your code>'''
# <include name of other modules used/imported>
# <include name of other modules used/imported>
# ---------------------------------------------------- #

# pendulum class


# example of interactive double pendulum 
# https://github.com/nesanders/Interactive-Double-Pendulum


import scipy
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import scipy.misc as misc
import random
from array import array
from numpy import sin, cos, pi, array
import scipy.integrate as integrate


class Pendulum():

    def __init__(self, state, pend_args, simulation_time, dt, links ):

        self.GG = 9.8
        
        self.state = state
        self.pend_args = pend_args

        self.theta1 = state[0] 
        self.omega1 = state[1]
        self.theta2 = state[2]
        self.omega2 = state[3]
        
        self.MM1 = pend_args[0]
        self.LL1 = pend_args[1]
        self.MM2 = pend_args[2]
        self.LL2 = pend_args[3]

        self.dt = dt
        self.t = np.arange(0.0, simulation_time, dt)

        if links == 2:    # Initialize variables needed for Double Pend 
                          # solve ODE's, transform to x-y coords, and save for later use
            self.solvz = integrate.odeint(self.get_derivs_dbl_pend, state, self.t)

            self.xx1, self.yy1 = self.cartesian_coord_from_radians(self.solvz)

            self.xx2 = self.LL2*sin(self.solvz[:,2]) + self.xx1
            self.yy2 = -self.LL2*cos(self.solvz[:,2]) + self.yy1
                  
        elif links == 1:  # Initialize variables needed for Simple Pend
                          # solve ODE's, transform to x-y coords, and save for later use
            self.simp_solvz = integrate.odeint(self.get_derivs_simp_pend, state, self.t)

            self.simp_xx1 = self.LL1*sin(self.simp_solvz[:,0])  
            self.simp_yy1 = -self.LL1*cos(self.simp_solvz[:,0])
            
        else:
            print('Error: Number of links can be ONLY 1 or 2. Please fix and try agian.')



# get derivatives for the double pendulum to use with the ODE function
    def get_derivs_dbl_pend(self, state, *other_args ):    # requires state as array and 

        theta1 = state[0]
        omega1 = state[1]
        theta2 = state[2]
        omega2 = state[3]

    # double pendulum equations from: http://www.physics.usyd.edu.au/~wheat/dpend_html/

        derivs = np.zeros_like(state)       # set up derivatives array to same size as 
                                            #     as the state array
        theta_dot1 = state[1]               # angular velocity of mass 1
        theta_dot2 = state[3]               # angular velocity of mass 2

        delta = state[2] - state[0]         # delta = theta1 - theta2
        denominator = (self.MM1 + self.MM2)-self.MM2*cos(delta)*cos(delta)
    
        theta_dot_dot1 = ( (self.MM2*self.LL1*omega1*omega1*sin(delta)
            *cos(delta)) +(self.MM2*self.GG*sin(theta2)*cos(delta)) + (self.MM2
            *self.LL2*omega2*omega2*sin(delta)) - (self.MM1+self.MM2)*(
            self.GG*sin(theta1)) ) / (self.LL1*denominator)
        
        theta_dot_dot2 = ( (-self.MM2*self.LL2*omega2*omega2*sin(delta)
            *cos(delta)) + (self.MM1+self.MM2)*(self.GG*sin(theta1)
            *cos(delta)) - (self.MM1+self.MM2)*(self.LL1*omega1*omega1
            *sin(delta)) - (self.MM1+self.MM2)*(self.GG
            *sin(theta2)) ) / (self.LL2*denominator)

        derivs = ( [ theta_dot1, theta_dot_dot1, theta_dot2, theta_dot_dot2] )
    
        return derivs



    def cartesian_coord_from_radians(self, ode_int_result):
        xx = self.LL1*sin(ode_int_result[:,0])
        yy = -self.LL1*cos(ode_int_result[:,0])
        
        return xx, yy


    def animate_dbl_pend( self , window):
    # http://matplotlib.org/users/pyplot_tutorial.html
        
        limz = self.LL1 + self.LL2

        fig = window
        ax = fig.add_subplot(111, autoscale_on=False, xlim=(-limz, limz), 
                              ylim=(-limz, limz))
        ax.grid()

        line, = ax.plot([], [], 'o-', lw=2)
        time_template = 'time = %.1fs'
        time_text = ax.text(0.05, 0.9, '', transform=ax.transAxes)

        def init():
            line.set_data([], [])
            time_text.set_text('')
            return line, time_text

        def animate(i):
            thisx = [0, self.xx1[i], self.xx2[i]]
            thisy = [0, self.yy1[i], self.yy2[i]]

            line.set_data(thisx, thisy)
            time_text.set_text(time_template%(i*self.dt))
            return line, time_text

        ani = animation.FuncAnimation(fig, animate, np.arange(1, len(self.solvz)),
            interval=25, blit=False, init_func=init)

        return ani

    def plot_dbl_pend_all_var(self):
        
        plt.subplot(221)
        plt.plot( self.t , self.solvz[:,0])
        plt.title( 'Theta1 vs Time' )
        #plt.xlabel( 'Time (s)')
        plt.ylabel( 'Theta1  (deg)')
    
        plt.subplot(222)
        plt.plot( self.t , self.solvz[:,1])
        plt.title( 'ThetaDot1 vs Time' )
        #plt.xlabel( 'Time (s)')
        plt.ylabel( 'ThetaDot1  (deg/sec)')

        plt.subplot(223)
        plt.plot( self.t , self.solvz[:,2])
        plt.title( 'Theta2 vs Time' )
        plt.xlabel( 'Time (s)')
        plt.ylabel( 'Theta2  (deg)')

        plt.subplot(224)
        plt.plot( self.t , self.solvz[:,3])
        plt.title( 'ThetaDot2 vs Time' )
        plt.xlabel( 'Time (s)')
        plt.ylabel( 'ThetaDot2  (deg/sec)')



    def plot_dbl_pend_chosen_var( self, chosen_var ):
    
        if chosen_var == 'Theta 1':
            window = plt.plot( self.t , self.solvz[:,0])
            plt.title( 'Theta1 vs Time' )
            plt.xlabel( 'Time (s)')
            plt.ylabel( 'Theta1  (deg)')
        elif chosen_var == 'Theta 2':
            window = plt.plot( self.t , self.solvz[:,2])
            plt.title( 'Theta2 vs Time' )
            plt.xlabel( 'Time (s)')
            plt.ylabel( 'Theta2  (deg)')
        elif chosen_var == 'Theta_Dot 1':
            window =  plt.plot( self.t , self.solvz[:,1])
            plt.title( 'ThetaDot1 vs Time' )
            plt.xlabel( 'Time (s)')
            plt.ylabel( 'ThetaDot1  (deg/sec)')
        elif chosen_var == 'Theta_Dot 2':
            window = plt.plot( self.t , self.solvz[:,3])
            plt.title( 'ThetaDot2 vs Time' )
            plt.xlabel( 'Time (s)')
            plt.ylabel( 'ThetaDot2  (deg/sec)')
        elif chosen_var == 'X Position - Mass 1':
            window = plt.plot( self.t , self.xx1)
            plt.title( 'X Position of Mass 1 vs Time' )
            plt.xlabel( 'Time (s)')
            plt.ylabel( 'X Position (m)')
        elif chosen_var == 'Y Position - Mass 1':
            window = plt.plot( self.t , self.yy1)
            plt.title( 'Y Position of Mass 1 vs Time' )
            plt.xlabel( 'Time (s)')
            plt.ylabel( 'Y Position (m)')
        elif chosen_var == 'X Position - Mass 2':
            window = plt.plot( self.t , self.xx2)
            plt.title( 'X Position of Mass 2 vs Time' )
            plt.xlabel( 'Time (s)')
            plt.ylabel( 'X Position (m)')
        elif chosen_var == 'Y Position - Mass 2':
            window = plt.plot( self.t , self.yy2)
            plt.title( 'Y Position of Mass 2 vs Time' )
            plt.xlabel( 'Time (s)')
            plt.ylabel( 'Y Position (m)')
        else:
            print('There is a problem here. Please select different parameter.')


    def get_derivs_simp_pend( self, state, *extra_args):    # *extra args ONLY becasue
                                                            #  odeint solver passes extra
        theta7 = state[0]                                   #  time arg that isn't needed
        omega7 = state[1]

        derivs = np.zeros_like(state)       # set up derivatives array to same size as 
                                            #     as the state array
        theta_dot7 = state[1]
        theta_dot_dot7 = - (self.GG/self.LL1)*sin(theta7)
    
        derivs = ( [ theta_dot7, theta_dot_dot7] )

        return derivs


    def animate_simp_pend( self, window ):
    
        limz = 2*self.LL1 
    
        fig = window
        ax = fig.add_subplot(111, autoscale_on=False, xlim=(-limz, limz), 
                                  ylim=(-limz, limz))
        ax.grid()

        line, = ax.plot([], [], 'o-', lw=2)
        time_template = 'time = %.1fs'
        time_text = ax.text(0.05, 0.9, '', transform=ax.transAxes)

        def init():
            line.set_data([], [])
            time_text.set_text('')
            return line, time_text

        def animate(i):
            thisx = [0, self.simp_xx1[i]]
            thisy = [0, self.simp_yy1[i]]

            line.set_data(thisx, thisy)
            time_text.set_text(time_template%(i*self.dt))
            return line, time_text

        ani = animation.FuncAnimation(fig, animate, np.arange(1, len(self.simp_solvz)),
            interval=25, blit=False, init_func=init)

        return ani

    def plot_simp_pend_all_var( self ):
    
    
        plt.subplot(121)
        plt.plot( self.t , self.simp_solvz[:,0])
        plt.title( 'Theta1 vs Time' )
        plt.xlabel( 'Time (s)')
        plt.ylabel( 'Theta1  (deg)')
    
        plt.subplot(122)
        plt.plot( self.t , self.simp_solvz[:,1])
        plt.title( 'ThetaDot1 vs Time' )
        plt.xlabel( 'Time (s)')
        plt.ylabel( 'ThetaDot1  (deg/sec)')



    def plot_simp_pend_chosen_var( self, chosen_var ):

        if chosen_var == 'Theta 1':
            plt.plot( self.t , self.simp_solvz[:,0])
            plt.title( 'Theta1 vs Time' )
            plt.xlabel( 'Time (s)')
            plt.ylabel( 'Theta1  (deg)')
        elif chosen_var == 'Theta 2':
            print('Error: Simple Pendulum has only 1 mass. No Theta 2 parameter to graph.')
        elif chosen_var == 'Theta_Dot 1':
            plt.plot( self.t , self.simp_solvz[:,1])
            plt.title( 'ThetaDot1 vs Time' )
            plt.xlabel( 'Time (s)')
            plt.ylabel( 'ThetaDot1  (deg/sec)')
        elif chosen_var == 'Theta_Dot 2':
            print('\n Error: Simple Pendulum has only 1 mass. \n\
            No Theta_Dot 2 parameter to graph.')
        elif chosen_var == 'X Position - Mass 1':
            plt.plot( self.t , self.simp_xx1)
            plt.title( 'X Position of Mass 1 vs Time' )
            plt.xlabel( 'Time (s)')
            plt.ylabel( 'X Position (m)')
        elif chosen_var == 'Y Position - Mass 1':
            plt.plot( self.t , self.simp_yy1)
            plt.title( 'Y Position of Mass 1 vs Time' )
            plt.xlabel( 'Time (s)')
            plt.ylabel( 'Y Position (m)')
        elif chosen_var == 'X Position - Mass 2':
            print('\n Error: Simple Pendulum has only 1 mass. \n\
            No X Position - Mass 2 parameter to graph.')
        elif chosen_var == 'Y Position - Mass 2':
            print('\n Error: Simple Pendulum has only 1 mass. \n\
            No Y Position - Mass 2 parameter to graph.')
        else:
            print('There is a problem here. Please select different parameter.')




# pend args

MM1 = 1 #2           # mass 1 [kg]
LL1 = 1 #3           # length of pendulum arm 1
MM2 = 1 #2.2         # mass 2 [kg]
LL2 = 1 #3.3         # length of pendulum arm 2

# initial values

theta1 = 120        # [degrees]
omega1 = 0         # angular velocity of mass 1    [degrees/second]
theta2 = -10         # [degrees]
omega2 = 0  

pend_args = ( MM1, LL1, MM2, LL2 )

state = np.array( [theta1, omega1, theta2, omega2] )
state = state.astype(np.float)
state *= pi/180



# 
# 
# # pend = Pendulum( state, pend_args, simulation_time, dt, number_of_links )
# pend = Pendulum(state, pend_args, 20, .05, 2)
# 
# # animate_dbl_pend( self )
# pend.animate_dbl_pend( )
# 
# # plot_dbl_pend_all_var(self)
# pend.plot_dbl_pend_all_var()
# 
# chosen_var = 'Y Position - Mass 2'
# # pend.plot_dbl_pend_chosen_var( self, chosen_var )
# pend.plot_dbl_pend_chosen_var( chosen_var )
# 
# 
# 
# # SIMPLE PENDULUM TESTING
# 
# # pend = Pendulum( state, pend_args, simulation_time, dt, number_of_links )
# pend = Pendulum(state, pend_args, 20, .05, 1)
# 
# # animate_simp_pend( self )
# pend.animate_simp_pend( )
# 
# 
# # plot_simp_pend_all_var( self )
# pend.plot_simp_pend_all_var(  )
# 
# chosen_var = 'Y Position - Mass 1'
# # plot_simp_pend_chosen_var( self, chosen_var )
# pend.plot_simp_pend_chosen_var( chosen_var )
# 
# 
# # get_derivs_simp_pend( self, state, *pend_args)
# simp_derivs = pend.get_derivs_simp_pend( state, *pend_args)
# print('these are the simp_derivs', simp_derivs)
# 
# 



'''
# get derivs for the ODE function
def get_derivs_dbl_pend( state, *pend_args):    # requires state as array and 
                                                # pendulum args as a tuple
    #print(pend_args)

    MM1 = pend_args[1]           # mass 1 [kg]
    LL1 = pend_args[2]           # length of pendulum arm 1
    MM2 = pend_args[3]           # mass 2 [kg]
    LL2 = pend_args[4]           # length of pendulum arm 2

#     print('PEND ARGS:', pend_args)
#     print('MM1:' , MM1)
#     print('LL1: ', LL1)
#     print('STATE:', state)
#     print('MM2:' , MM2)
#     print('LL2: ', LL2)
    theta1 = state[0]
    omega1 = state[1]
    theta2 = state[2]
    omega2 = state[3]

    # double pendulum equations from: http://www.physics.usyd.edu.au/~wheat/dpend_html/

    derivs = np.zeros_like(state)       # set up derivatives array to same size as 
                                        #     as the state array
    theta_dot1 = state[1]               # angular velocity of mass 1
    theta_dot2 = state[3]               # angular velocity of mass 2

    delta = state[2] - state[0]         # delta = theta1 - theta2
    denominator = (MM1 + MM2) - MM2*cos(delta)*cos(delta)
    
    theta_dot_dot1 = ( (MM2*LL1*omega1*omega1*sin(delta)*cos(delta)) +(MM2*GG*
        sin(theta2)*cos(delta)) + (MM2*LL2*omega2*omega2*sin(delta)) - (MM1+MM2)*(
        GG*sin(theta1)) ) / (LL1*denominator)
        
    theta_dot_dot2 = ( (-MM2*LL2*omega2*omega2*sin(delta)*cos(delta)) + (MM1+MM2)*(GG*
        sin(theta1)*cos(delta)) - (MM1+MM2)*(LL1*omega1*omega1*sin(delta)) - (MM1+MM2)*(
        GG*sin(theta2)) ) / (LL2*denominator)

    derivs = ( [ theta_dot1, theta_dot_dot1, theta_dot2, theta_dot_dot2] )
    
    return derivs






'''