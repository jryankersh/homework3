

# ----------------------------------------------------------------------------
# GUI
# ----------------------------------------------------------------------------
# set up buttons
# set up sliders
# set up value entry boxes
# set up drop down menu
# set up check boxes
# 
# draw canvas for plots and animations
# 
# define function to get values from all of the widgets (sliders, entries, etc...)
#     use these values to build the inputs for (initialize) pendulum class/functions
#     get value of the checkbox
#         if checked, then run double pendulum functions
#         if unchecked, then run simple pendulum functions
# 
# define run button action that starts the animation
#     get values/refresh values from each of the sliders, entry boxes, etc..
#     pass those values to pendulum class/animation function
# 
# define plot button action that plots pendulum motion
#     get values/refresh values from each of the sliders, entry boxes, etc..
#     pass those values to pendulum class/plot function
# 
# define action for the drop down menu selection
#     get values/refresh values from each of the sliders, entry boxes, etc..
#     check drop down selection to determine which value to plot seperately
#     pass those values to pendulum class/plot function
# 
# define stop button action to end the GUI program
# ----------------------------------------------------------------------------



#----------------------------------------------------------------------------
#Pendulum Solver
#----------------------------------------------------------------------------


# Initialize pendulum variables with inputs from the GUI
#     use values from GUI to initialize variables needed for pendulum
#     check for number of links to determine which functions to use
#     use a derivatives function w/ ODE solver & store results for use in other functions
#     use ODE solver results and transform radians to x-y coords
#     store all initialized variables for later use in other functions
#         (might be good to use a class for this)



# --For Double Pendulum--
#     define function to get derivatives for the double pendulum
#         (setting up equations for theta_dot and theta_dot_dot for masses 1 and 2)
#     use derivatives function w/ ODE solver and store results for use in other functions

#     Animate double pendulum function
#          use results from ODE solver

#     Plot variables from simulation
#          use results form ODE solver to plot theta1, theta2, thetadot_1, 
#              thetadot_2, etc... with time
#          if a variable was chosen from the GUI dropdown menu to plot by itself,
#              use if statements to determine correct plot


# --For Simple Pendulum--
#     define function to get derivatives for the simple pendulum
#         (setting up equations for theta_dot and theta_dot_dot for mass 1)
#     use derivatives function w/ ODE solver and store results for use in other functions

#     Animate double pendulum function
#          use results from ODE solver

#     Plot variables from simulation
#          use results form ODE solver to plot theta1, theta2, thetadot_1, 
#              thetadot_2, etc... with time
#          if a variable was chosen from the GUI dropdown menu to plot by itself,
#              use if statements to determine correct plot













    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
